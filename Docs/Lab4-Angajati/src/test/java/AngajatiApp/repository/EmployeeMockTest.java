package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.controller.EmployeeController;
import AngajatiApp.model.Employee;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeMockTest {
    EmployeeMock repository;
    EmployeeController controller;

    @BeforeEach
    public void setUp() {
        repository = new EmployeeMock();
    }

    @Test
    void modifyEmployeeFunction_TC1(){
        try {
            Employee employee = new Employee("Ion", "Dumitrescu", "1234567890876", DidacticFunction.LECTURER, 2500d);
            employee = null;
            repository.addEmployee(employee);

            repository.modifyEmployeeFunction(employee, DidacticFunction.ASISTENT);
            System.out.println(employee.toString());
            assertEquals(employee.getFunction(), DidacticFunction.ASISTENT);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    @Test
    void modifyEmployeeFunction_TC2(){
        Employee employee = new Employee("Ion", "Dumitrescu", "1234567890876", DidacticFunction.LECTURER, 2500d);
        repository.getEmployeeList().clear();

        repository.addEmployee(employee);

        repository.modifyEmployeeFunction(employee, DidacticFunction.ASISTENT);
        System.out.println(repository.getEmployeeList().get(0));

        assertEquals(employee.getFunction(), DidacticFunction.ASISTENT);
    }



    @Test
    void modifyEmployeeFunction_TC3(){
        Employee employee = new Employee("Ion", "Dumitrescu", "1234567890876", DidacticFunction.LECTURER, 2500d);

        repository.addEmployee(employee);

        repository.modifyEmployeeFunction(employee, DidacticFunction.ASISTENT);
        System.out.println(employee.toString());
        assertEquals(employee.getFunction(), DidacticFunction.ASISTENT);
    }
}