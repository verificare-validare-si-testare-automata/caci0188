package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.controller.EmployeeController;
import AngajatiApp.model.Employee;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeMockTest {
    EmployeeMock repo;
    @BeforeEach

    public void setUp(){
        repo= new EmployeeMock();
    }
    // test with valid data
    @Test
    void addEmployeeTC1_BB() {
        int actualEmployeeCount = repo.getEmployeeList().size();

        try {
            Employee employee = new Employee("Craiu", "Andrei", "1987454474151", DidacticFunction.PROGRAMMER, 2541d);
               repo.addEmployee(employee);
               assertEquals(actualEmployeeCount+1, repo.getEmployeeList().size());

        }catch (Exception e){
            System.out.println(e.getMessage());

        }

    }

    // test with non-valid data
    @Test
    void addEmployeeTC2_BB() {
        int actualEmployeeCount = repo.getEmployeeList().size();

        try {
            Employee employee = new Employee(null, "Andrei", "1987454474151", DidacticFunction.PROGRAMMER, 2541d);
                repo.addEmployee(employee);
                assertEquals(actualEmployeeCount+1, repo.getEmployeeList().size());
        }catch (Exception e){
            e.printStackTrace();
        }

    }
    // test with non-valid data
    @Test
    void addEmployeeTC3_BB() {
        int actualEmployeeCount = repo.getEmployeeList().size();

        try {
            Employee employee = new Employee("", "Andrei", "1987454474151", DidacticFunction.PROGRAMMER, 2541d);
                repo.addEmployee(employee);
                assertEquals(actualEmployeeCount + 1, repo.getEmployeeList().size());

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    // test with non-valid data
    @Test
    void addEmployeeTC4_BB() {
        int actualEmployeeCount = repo.getEmployeeList().size();

        try {
            Employee employee = new Employee("Annnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn", "Andrei", "1987454474151", DidacticFunction.PROGRAMMER, 2541d);

                assertFalse( repo.addEmployee(employee));
                assertEquals(actualEmployeeCount, repo.getEmployeeList().size());

        }catch (Exception e){
            e.printStackTrace();

        }

    }

    // test with non-valid data
    @Test
    void addEmployeeTC5_BB() {
        int actualEmployeeCount = repo.getEmployeeList().size();

        try {
            Employee employee = new Employee("Craiu", null, "1987454474151", DidacticFunction.PROGRAMMER, 2541d);

                repo.addEmployee(employee);
                assertEquals(actualEmployeeCount+1, repo.getEmployeeList().size());

        }catch (Exception e){
            e.printStackTrace();

        }

    }
    // test with non-valid data
    @Test
    void addEmployeeTC6_BB() {
        int actualEmployeeCount = repo.getEmployeeList().size();

        try {
            Employee employee = new Employee("Craiu", " ", "1987454474151", DidacticFunction.PROGRAMMER, 2541d);

                repo.addEmployee(employee);
                assertEquals(actualEmployeeCount+1, repo.getEmployeeList().size());

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    // test with non-valid data
    @Test
    void addEmployeeTC7_BB() {
        int actualEmployeeCount = repo.getEmployeeList().size();

        try {
            Employee employee = new Employee("Craiu", "Andrei", "1987454474151", DidacticFunction.PROGRAMMER, 254147d);

                repo.addEmployee(employee);
                assertEquals(actualEmployeeCount+1, repo.getEmployeeList().size());

        }catch (Exception e){
            e.printStackTrace();

        }

    }

    // test with non-valid data
    @Test
    void addEmployeeTC8_BB() {
        int actualEmployeeCount = repo.getEmployeeList().size();

        try {
            Employee employee = new Employee("Craiu", "Andrei", "1987454474151", DidacticFunction.PROGRAMMER, 0d);

                repo.addEmployee(employee);
                assertEquals(actualEmployeeCount+1, repo.getEmployeeList().size());
        }catch (Exception e){
            e.printStackTrace();

        }

    }
    // test with non-valid data
    @Test
    void addEmployeeTC9_BB() {
        int actualEmployeeCount = repo.getEmployeeList().size();

        try {
            Employee employee = new Employee("Craiu", "Andrei", "1987454474151", DidacticFunction.PROGRAMMER, 4000001d);

                repo.addEmployee(employee);
                assertEquals(actualEmployeeCount+1, repo.getEmployeeList().size());
        }catch (Exception e){
           e.printStackTrace();

        }

    }
}

